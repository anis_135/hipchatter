﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Media;

namespace HipChat
{
    public class OnlineStateDescriptionConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string culture)
        {
            var state = (OnlineState)value;

            switch (state)
            {
                case OnlineState.Online:
                case OnlineState.Chat:
                    return "Available (online)";
                case OnlineState.Offline:
                    return "Not available (offline)";
                case OnlineState.Away:
                case OnlineState.ExtendedAway:
                    return "Away";
                case OnlineState.DoNotDisturb:
                    return "Busy (do not disturb)";
                case OnlineState.OnMobileApple:
                case OnlineState.OnMobileAndroid:
                    return "Available on mobile";
            }

            return new SolidColorBrush(Colors.LightGray);
        }

        public object ConvertBack(object value, Type targetType, object parameter, string culture)
        {
            return null;
        }
    }
}
